package lab1;

import java.util.ArrayList;
import java.util.List;

public class Node {
	int state;
	List<Node> neightbour = new ArrayList<>();
	boolean visited = false;
	Node parent = null;

	public Node(int state) {
		this.state = state;
	}

	public void addNeighbour(Node n) {
		neightbour.add(n);
	}

	public List<Node> getNeighbour() {
		return neightbour;
	}
}
