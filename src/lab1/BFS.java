package lab1;

import java.util.LinkedList;
import java.util.Queue;

public class BFS {
	Queue<Node> frontier = new LinkedList<>();

	public void bfsUsingQueue(Node initial, int goal) {
		if (initial.state == goal) {
			initial.visited = true;
			initial.parent = null;
			System.out.print(printPath(initial));
			return;
		}

		frontier.add(initial);
		while (!frontier.isEmpty()) {
			Node s = frontier.poll();
			s.visited = true;

			for (int i = 0; i < s.neightbour.size(); i++) {
				Node v = s.neightbour.get(i);

				if (v.state == goal) {
					v.visited = true;
					v.parent = s;
					System.out.println(printPath(v));
					return;
				}

				if (!v.visited) {
					v.parent = s;
					frontier.add(v);
				}
			}
		}

	}

	private String printPath(Node goal) {
		if (goal.parent == null) {
			return goal.state + "";
		} else {
			return printPath(goal.parent) + " => " + goal.state;
		}
	}

	public static void main(String[] args) {
		Node node40 = new Node(40);
		Node node10 = new Node(10);
		Node node20 = new Node(20);
		Node node30 = new Node(30);
		Node node60 = new Node(60);
		Node node50 = new Node(50);

		Node node70 = new Node(70);

		node40.addNeighbour(node10);
		node40.addNeighbour(node20);
		node10.addNeighbour(node30);
		node20.addNeighbour(node10);
		node20.addNeighbour(node30);
		node20.addNeighbour(node50);
		node20.addNeighbour(node60);
		node30.addNeighbour(node60);
		node60.addNeighbour(node70);
		node50.addNeighbour(node70);

		System.out.println("The BFS traversal of the graph using queue ");
		BFS bfs = new BFS();
		bfs.bfsUsingQueue(node40, 70);

	}
}
